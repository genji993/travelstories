class MapsController < ApplicationController
	def index

    current_year = Date.current.year.to_i;
    delta = 20;

    from = current_year;
    to = current_year-delta;

    @datesForTravels = [];
    for i in from.downto(to)
      @datesForTravels.push(i);
    end

    to = current_year+delta;

    @datesForNextTravels = [];
    for i in from+1..to
      @datesForNextTravels.push(i);
    end

		if(params[:id].present?)




			@user = User.find_by_id(params[:id])

      @travel = Travel.new
      @next_travel = NextTravel.new

			@travels = Travel.where(user_id: params[:id])
			@ntravels = NextTravel.where(user_id: params[:id])



			if (@travels!=nil)
				@hasht = Gmaps4rails.build_markers(@travels) do |travel, marker|
 					marker.lat travel.latitude
  					marker.lng travel.longitude
  					marker.infowindow "È stato in "+travel.location+" nel "+travel.date.to_s
  					marker.picture({
                  		:url => view_context.image_path("green_marker.png"),
                  		:width   => 32,
                  		:height  => 32
                 	})
  					

  			end 

			end

			if (@ntravels!=nil)
				@hashnt = Gmaps4rails.build_markers(@ntravels) do |ntravel, marker|
 					marker.lat ntravel.latitude
  					marker.lng ntravel.longitude
  					marker.infowindow "Vorrebbe andare in "+ntravel.location+" nel "+ntravel.date.to_s
  					marker.picture({
                  		:url => view_context.image_path("yellow_marker.png"),
                  		:width   => 32,
                  		:height  => 32
                 	})

  				end
			end

  		end
	end	
end
