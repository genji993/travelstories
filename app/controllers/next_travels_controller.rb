class NextTravelsController < ApplicationController
  before_action :set_next_travel, only: [:show, :edit, :update, :destroy]

  # GET /next_travels
  # GET /next_travels.json
  def index
    @next_travels = NextTravel.all
  end

  # GET /next_travels/1
  # GET /next_travels/1.json
  def show
  end

  # GET /next_travels/new
  def new
    @next_travel = NextTravel.new
  end

  # GET /next_travels/1/edit
  def edit
  end

  # POST /next_travels
  # POST /next_travels.json
  def create
    @next_travel = NextTravel.new(next_travel_params)
    @next_travel.user_id = current_user.id

    respond_to do |format|
      if @next_travel.save
        format.html { redirect_to :controller=>:maps,:action=>:index,:id=>current_user.id }
        format.json { render :show, status: :created, location: @next_travel }
      else
        format.html { render :new }
        format.json { render json: @next_travel.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /next_travels/1
  # PATCH/PUT /next_travels/1.json
  def update
    respond_to do |format|
      if @next_travel.update(next_travel_params)
        format.html { redirect_to :controller=>:users,:action=>:user_profile,:id=>current_user.id }
        format.json { render :show, status: :ok, location: @next_travel }
      else
        format.html { render :edit }
        format.json { render json: @next_travel.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /next_travels/1
  # DELETE /next_travels/1.json
  def destroy
    @next_travel.destroy
    respond_to do |format|
      format.html { redirect_to :controller=>:users,:action=>:user_profile,:id=>current_user.id }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_next_travel
      @next_travel = NextTravel.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def next_travel_params
      params.require(:next_travel).permit(:location, :date, :user_id)
    end
end
