class PhotosController < ApplicationController
  
  before_action :set_photo, only: [:edit, :update, :destroy]
  
  # GET /photos
  # GET /photos.json
  def index
    if params[:id].present?
      @photo = Photo.new
      @user = User.find(params[:id])

      @photos = Photo.where(user_id: params[:id]).order("created_at DESC").paginate(page: params[:page], per_page: 8)
    
    end

    if params[:pid].present?  
      @photo = Photo.find(params[:pid].to_i)
    end  
     
  end

  # GET /photos/1
  # GET /photos/1.json
  def show
    if(params[:uid].present?)

      photos = Photo.where(user_id: params[:uid]);
      

    elsif(params[:location].present?)
      
      photos = Photo.where(location: params[:location]);

    else  
      photos = Photo.all.limit(18).order("created_at DESC")
      
    end  
        
        
    array=[]
    photos.each do |photo|
      array.push(photo.as_json.merge(:url => photo.image.url))
    end  
    
    render json: array
    
  end

  # GET /photos/new
  def new
    @photo = Photo.new
  end

  # GET /photos/1/edit
  def edit
  end

  # POST /photos
  # POST /photos.json
  def create
    @photo = Photo.new(photo_params)
    @photo.user_id = current_user.id
  
    respond_to do |format|
      if @photo.save
        format.html { redirect_to photos_path(id: current_user.id), notice: 'Photo was successfully created.' }
        format.json { render :index, status: :created, location: @photo }
      else
        format.html { redirect_to photos_path(id: current_user.id), alert: 'Fill in all fields' }
        format.json { render json: @photo.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /photos/1
  # PATCH/PUT /photos/1.json
  def update
    respond_to do |format|
      if @photo.update(photo_params)
        format.html { redirect_to photosOf_path(id: current_user.id), notice: 'Photo was successfully updated.' }
        format.json { render :show, status: :ok, location: @photo }
      else
        format.html { render :edit }
        format.json { render json: @photo.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /photos/1
  # DELETE /photos/1.json
  def destroy
    @photo.destroy
    respond_to do |format|
      format.html { redirect_to photosOf_path(id: current_user.id), notice: 'Photo was successfully removed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_photo
      @photo = Photo.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def photo_params
      params.require(:photo).permit(:user_id,:image,:description,:location)
    end

end
