class PostsController < ApplicationController
  before_action :set_post, only: [:show, :edit, :update, :destroy]



  # GET /posts
  # GET /posts.json
  def index
    if params[:id].present?
      @user = User.find(params[:id].to_i)
      @posts = Post.where(user_id: params[:id].to_i).paginate(page: params[:page], per_page: 5)
    end  
  end  

    
  

  # GET /posts/1
  # GET /posts/1.json
  def show
  end

  # GET /posts/new
  def new
    @post = Post.new
  end

  # GET /posts/1/edit
  def edit
  end

  # POST /posts
  # POST /posts.json
  def create
    @post = Post.new(post_params)

    @post.user_id = current_user.id.to_i

    respond_to do |format|
      if @post.save
        format.html { redirect_to :controller => :users,:action=>:user_profile, :id=>current_user.id }
        format.json { render :show, status: :created, location: @post }
      else
        format.html { redirect_to user_profile_path(id: current_user.id), alert: "Attention: Fill in all fields. Title Maximum Length: 30 characters. Description maximum length 80 characters. Location length 2-15 characters."}
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /posts/1
  # PATCH/PUT /posts/1.json
  def update
    respond_to do |format|
      if @post.update(post_params)
        format.html { redirect_to :controller => :users,:action=>:user_profile, :id=>current_user.id}
        format.json { render :show, status: :ok, location: @post }
      else
        format.html { render :edit }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /posts/1
  # DELETE /posts/1.json
  def destroy
    @post.destroy
    respond_to do |format|
      format.html { redirect_to :controller => :users,:action=>:user_profile, :id=>current_user.id }
      format.json { head :no_content }
    end
  end

  def body
    if(params[:articolo].present?)
      @articolo = Post.find_by_titolo(params[:articolo])
    end  
  end 


  def list
      @posts = Post.all()
      
  end  



  private
    # Use callbacks to share common setup or constraints between actions.
    def set_post
      @post = Post.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def post_params
      params.require(:post).permit(:titolo, :body, :user_id,:description,:location)
    end



end




