class ProfilesController < ApplicationController
  before_action :set_profile, only: [:show, :edit, :update, :destroy]



  # GET /profiles
  # GET /profiles.json
  def index
    @profiles = Profile.all
  end

  # GET /profiles/1
  # GET /profiles/1.json
  def show
  end

  # GET /profiles/new
  def new
    @profile = Profile.new
    @sexs = ['M'], ['F']
  end

  # GET /profiles/1/edit
  def edit
  end

  # POST /profiles
  # POST /profiles.json
  def create
    @profile = Profile.new(profile_params)
    @sexs = ['M'], ['F']
    
    @profile.user_id = current_user.id
    respond_to do |format|
      if @profile.save
        format.html { redirect_to :controller=>:users,:action=>:user_profile,:id=>current_user.id }
        format.json { render :show, status: :created, location: @profile }
      else
        format.html { render :new }
        format.json { render json: @profile.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /profiles/1
  # PATCH/PUT /profiles/1.json
  def update
    
    respond_to do |format|
      if @profile.update(profile_params)
        format.html { redirect_to :controller=>:users,:action=>:user_profile,:id=>current_user.id }
        format.json { render :show, status: :ok, location: @profile }
      else
        format.html { render :edit }
        format.json { render json: @profile.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /profiles/1
  # DELETE /profiles/1.json
  def destroy
    @profile.destroy
    respond_to do |format|
      format.html { redirect_to homepage_path() }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_profile
      @sexs = ['M'], ['F']
      @profile = Profile.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def profile_params

      params.require(:profile).permit(:user_id,:avatar,:username, :nome, :cognome, :gender, :data_nascita, :luogo_nascita, :citta_attuale, :biografia)
    end
end
