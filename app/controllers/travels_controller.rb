class TravelsController < ApplicationController
  before_action :set_travel, only: [:show, :edit, :update, :destroy]
  respond_to :json

  # GET /travels
  # GET /travels.json
  def index

    if params[:year].present?
      @travels = Travel.find_by_sql("
          SELECT id,location,date,count(location) AS count 
          FROM travels
          WHERE date="+params[:year].to_s+"
          GROUP BY location
          ORDER BY count DESC
        ");



    render json: @travels

    end




  end


 def date
  @dates = Travel.find_by_sql("
    SELECT date AS year
    FROM travels
    GROUP BY date
    ORDER BY date DESC
    ");
  render json: @dates
 end



  # GET /travels/1
  # GET /travels/1.json
  def show
  end

  # GET /travels/new
  def new
    @travel = Travel.new
  end

  # GET /travels/1/edit
  def edit
  end

  def analytics


  end


  # POST /travels
  # POST /travels.json
  def create


    @travel = Travel.new(travel_params)
    @travel.user_id = current_user.id


    if @travel.date <= Time.current.year

      respond_to do |format|
        if @travel.save
          format.html { redirect_to :controller=>:maps,:action=>:index,:id=>current_user.id }
          format.json { render :show, status: :created, location: @travel }
        else
          format.html { render :new }
          format.json { render json: @travel.errors, status: :unprocessable_entity }
        end
      end
    end

end

  # PATCH/PUT /travels/1
  # PATCH/PUT /travels/1.json
  def update
    respond_to do |format|
      if @travel.update(travel_params)
        format.html { redirect_to :controller=>:users,:action=>:user_profile,:id=>current_user.id }
        format.json { render :show, status: :ok, location: @travel }
      else
        format.html { render :edit }
        format.json { render json: @travel.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /travels/1
  # DELETE /travels/1.json
  def destroy
    @travel.destroy
    respond_to do |format|
      format.html { redirect_to :controller=>:users,:action=>:user_profile,:id=>current_user.id }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_travel
      @travel = Travel.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def travel_params
      params.require(:travel).permit(:location, :date,:user_id)
    end
end
