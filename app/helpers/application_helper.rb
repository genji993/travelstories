module ApplicationHelper

    def full_title(page_title)
      base_title = "Travel Stories"
      if page_title.empty?
        base_title
      else
        "#{page_title} | #{base_title}"
      end
    end


	  def resource_name
    	:user
  	end

  	def resource
    	@resource ||= User.new
  	end

  	def devise_mapping
    	@devise_mapping ||= Devise.mappings[:user]
  	end


  	def last_post_of(id)
  		last_post = Post.where(user_id: id).last
      return last_post
  	end	

    def get_same_NextTravelLocation_of(id)
      location = NextTravel.find_by_sql("SELECT location FROM next_travels WHERE user_id="+id.to_s+" AND location IN (SELECT location FROM next_travels WHERE user_id="+current_user.id.to_s+") GROUP BY location")
      return location
    end  

    def active_page(active_page)
      @active == active_page ? "active" : ""
    end

    def PrintOrEditField(param_id,param,str)
      if !param.blank?
        if params[:id].to_i == current_user.id
          link_to param,edit_photo_path(id: param_id.to_i)
        else 
          param
        end 
      else
        if params[:id].to_i == current_user.id
          link_to str,edit_photo_path(id: param_id.to_i)
        end  
      end  
    end  







    def get_next_pid_of(uid,pid)

      photo = Photo.find(pid)
      date = photo.created_at
      next_photo = Photo.where("created_at > ?",date).order("created_at DESC")
      next_photo = next_photo.first

      if next_photo != nil

        return next_photo.id
      else
      
        return Photo.first.id
      end  


    end  




     def get_prev_pid_of(uid,pid)

      photo = Photo.find(pid)
      date = photo.created_at
      prev_photo = Photo.where("created_at < ?",date).order("created_at DESC")
      prev_photo = prev_photo.first

      if prev_photo != nil

        return prev_photo.id
      else
      
        return Photo.last.id
      end  


    end 




  

    

end
