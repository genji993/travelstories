class Post < ActiveRecord::Base

	belongs_to :user
	
	has_many :comments, dependent: :destroy
	

	validates :titolo, presence: true, allow_blank: false,length: { maximum: 30 }
	validates :description, presence: true, allow_blank: false,length: { maximum: 80 }
	validates :location, presence: true, allow_blank: false,length: { maximum: 30, minimum: 2 }
	validates :body, presence: true, allow_blank: false
	
	ratyrate_rateable "body"

end
