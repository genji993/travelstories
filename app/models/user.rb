class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         :database_authenticatable, :confirmable


  has_many :posts, dependent: :destroy
  has_many :photos, dependent: :destroy
  
  has_many :comments, dependent: :destroy
  has_one :profile, dependent: :destroy

  has_many :travels, dependent: :destroy
  has_many :nextTravels, dependent: :destroy

  acts_as_messageable

  ratyrate_rater

  # in User.rb
  protected
  def confirmation_required?
    false
  end
 
  def mailboxer_name
    self.id
  end
 
  def mailboxer_email(object)
    self.email
  end


end
