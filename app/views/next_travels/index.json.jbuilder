json.array!(@next_travels) do |next_travel|
  json.extract! next_travel, :id, :location, :date, :user_id
  json.url next_travel_url(next_travel, format: :json)
end
