json.posts @posts do |post|

	if post.user_id == current_user.id

		json.id post.id
		json.title post.titolo
		json.user post.user.profile.username

		json.comments post.comments do |comment|

			json.id comment.id
			json.body comment.body
			json.user comment.user.profile.username

		end

	end






end