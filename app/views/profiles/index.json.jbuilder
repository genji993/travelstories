json.array!(@profiles) do |profile|
  json.extract! profile, :id, :username, :nome, :cognome, :gender, :data_nascita, :luogo_nascita, :citta_attuale, :biografia, :motto
  json.url profile_url(profile, format: :json)
end
