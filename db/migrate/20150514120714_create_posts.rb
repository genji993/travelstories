class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.string :titolo
      t.string :body
      t.string :autore

      t.timestamps null: false
    end
  end
end
