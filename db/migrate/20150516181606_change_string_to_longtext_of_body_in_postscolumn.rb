class ChangeStringToLongtextOfBodyInPostscolumn < ActiveRecord::Migration
  def change
  	change_column :posts, :body,  :longtext
  end
end
