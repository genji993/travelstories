class CreateProfiles < ActiveRecord::Migration
  def change
    create_table :profiles do |t|
      
      t.string  :username, limit: 15
      t.string  :nome, limit: 30
      t.string  :cognome, limit: 15
      t.string  :gender, limit: 1
      t.date    :data_nascita
      t.string  :luogo_nascita, limit: 30
      t.string  :citta_attuale, limit: 30
      t.string  :biografia, limit: 150
      
      t.timestamps null: false
    end
  end
end
