class ChangeMottoLimit < ActiveRecord::Migration
  def change
  	change_column :profiles, :motto, :string, :limit => 150
  end
end
