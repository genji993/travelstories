class CreateTravels < ActiveRecord::Migration
  def change
    create_table :travels do |t|
      t.string :location
      t.integer :user_id

      t.timestamps null: false
    end
  end
end
