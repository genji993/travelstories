class AddDateColumnToTravel < ActiveRecord::Migration
  def change
  	add_column :travels, :date, :year
  end
end
