class CreateNextTravels < ActiveRecord::Migration
  def change
    create_table :next_travels do |t|
      t.string :location
      t.integer :user_id

      t.timestamps null: false
    end
  end
end
