class AddLatitudeLongitudeToNextTravel < ActiveRecord::Migration
  def change
    add_column :next_travels, :latitude, :float
    add_column :next_travels, :longitude, :float
  end
end
