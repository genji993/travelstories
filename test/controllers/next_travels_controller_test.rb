require 'test_helper'

class NextTravelsControllerTest < ActionController::TestCase
  setup do
    @next_travel = next_travels(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:next_travels)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create next_travel" do
    assert_difference('NextTravel.count') do
      post :create, next_travel: { date: @next_travel.date, location: @next_travel.location, user_id: @next_travel.user_id }
    end

    assert_redirected_to next_travel_path(assigns(:next_travel))
  end

  test "should show next_travel" do
    get :show, id: @next_travel
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @next_travel
    assert_response :success
  end

  test "should update next_travel" do
    patch :update, id: @next_travel, next_travel: { date: @next_travel.date, location: @next_travel.location, user_id: @next_travel.user_id }
    assert_redirected_to next_travel_path(assigns(:next_travel))
  end

  test "should destroy next_travel" do
    assert_difference('NextTravel.count', -1) do
      delete :destroy, id: @next_travel
    end

    assert_redirected_to next_travels_path
  end
end
